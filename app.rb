require 'sinatra'
require 'socket'

class JIMSocket
  hostname = 'localhost'
  port = 4004

  def connect
=begin
  Unfortunately using variables here isn't working for me, this is not how this
  should be done.
=end
    @socket = TCPSocket.open 'localhost', 4004
  end

  def listen
    while line = @socket.gets
      $buffer << line.chop
    end
  end
end

class App < Sinatra::Base
  enable :sessions
  set :session_secret, 'IC710iZYzsfbBWK955sBUD8u4VP0O177'
  page_title = 'JIM Web Client'

=begin
  Like the buffer this really should not be a global variable but setting it as
  a class or instance variable seems to be breaking. I'd really like to
  understand why
=end
  $socket = JIMSocket.new
=begin
  This should not be a global variable but I was breaking it as a class
  variable so I thought I'd give it a shot.
=end
  $buffer = Array.new

  get '/' do
    @title = page_title
    session[:value] = 'index'
    erb :index
  end

  get '/about' do
    @title = page_title
    session[:value] = 'about'
    erb :about
  end
=begin
  I'm wondering if part of this functionality should be moved into patch or put
  but when I tried doing so it didn't work properly.
=end
  post '/client' do
    @title = page_title

    if session[:value] != 'client'
      $buffer << "Username #{params[:user]}"
      $socket.connect
      $buffer << 'Connection started...'
      session[:value] = 'client'
    else
      $buffer << params[:message]
    end

    erb :client
  end
end
